.. _api:

API (перевод на начальной стадии)
=================================

.. module:: flask

Эта часть документации содежит описание всех интерфейсов Flask.  В тех
местах, где Flask зависит от внешних библиотек, мы документируем самое
важное прямо здесь, а также снабжаем вас ссылкой на оригинальную
документацию.


Объект приложения
-----------------

.. autoclass:: Flask
   :members:
   :inherited-members:


Объекты blueprint
-----------------

.. autoclass:: Blueprint
   :members:
   :inherited-members:

Данные входящих запросов
------------------------

.. autoclass:: Request
   :members:

   .. attribute:: form

      :class:`~werkzeug.datastructures.MultiDict` с данными разбора от запросов
      `POST` или `PUT`.  Помните, пожалуйста, что загрузки файлов на этом не
      ограничиваются, есть ещё атрибут :attr:`files`.

   .. attribute:: args

      :class:`~werkzeug.datastructures.MultiDict` с разобранным содержимым
      строки запроса.  (Это та часть URL, которая следует после знака вопроса).

   .. attribute:: values

      :class:`~werkzeug.datastructures.CombinedMultiDict` с содержимым
      :attr:`form` и :attr:`args`.

   .. attribute:: cookies

      :class:`dict` с содержимым всех переменных браузера, переданных с
      запросом.

   .. attribute:: stream

      Если поступающие данные формы не закодированы одним из известных
      типов mimetype, потребляемые данные сохраняются в этом потоке в
      неизменном виде.  В большинстве случаев лучше использовать
      :attr:`data`, который предоставит вам эти данные в виде строки.
      Поток возвращает данные лишь единожды.

   .. attribute:: headers

      Заголовки поступившего запроса в виде словаря, как объекта.

   .. attribute:: data

      Содержит данные поступившего запроса в виде строки в том случае, когда
      Flask не может обработать такой mimetype.

   .. attribute:: files

      :class:`~werkzeug.datastructures.MultiDict` с файлами, загружаемыми как часть
      запроса `POST` или `PUT`.  Каждый файл сохраняется в виде объекта
      :class:`~werkzeug.datastructures.FileStorage`.  Он, говоря простым языком,
      ведёт себя как известный вам по Python объект файла, с той разницей, что
      в нём есть функция :meth:`~werkzeug.datastructures.FileStorage.save`, которая
      может сохранить файл в файловую систему..

   .. attribute:: environ

      Нижележащее WSGI-окружение.

   .. attribute:: method

      Текущий метод запроса (``POST``, ``GET`` и т. д.)

   .. attribute:: path
   .. attribute:: script_root
   .. attribute:: url
   .. attribute:: base_url
   .. attribute:: url_root

      Предоставляет различные способы представления текущего URL.  Представим, что
      ваше приложение соответствует следующему URL::

          http://www.example.com/myapplication

      А запросы пользователя поступают на такой URL::

          http://www.example.com/myapplication/page.html?x=y

      В таком случае значения вышеупомянутых атрибутов станут вот такими:

      ============= ======================================================
      `path`        ``/page.html``
      `script_root` ``/myapplication``
      `base_url`    ``http://www.example.com/myapplication/page.html``
      `url`         ``http://www.example.com/myapplication/page.html?x=y``
      `url_root`    ``http://www.example.com/myapplication/``
      ============= ======================================================

   .. attribute:: is_xhr

      `True` если запрос обработан через JavaScript `XMLHttpRequest`.
      Это работает лишь с теми библиотеками, которые поддерживают
      заголовок ``X-Requested-With`` и устанавливают его в значение
      `XMLHttpRequest`.  Подобными библиотеками являются prototype,
      jQuery и Mochikit, возможно какие-то ещё.

.. class:: request

   Чтобы получить доступ к данным поступившего запроса, вы можете
   использовать глобальный объект `request`.  Flask занимается для вас
   разбором данных поступившего запроса и предоставляет вам к ним доступ
   через глобальный объект.  Внутри себя Flask хочет убедиться, что
   в случае многопоточного окружения, вы всегда получите корректные
   данные, адресованные активному потоку.

   Это прокси.  За дополнительными сведениями обращайтесь к
   :ref:`notes-on-proxies`.

   Объект запроса это экземпляр подкласса
   :class:`~werkzeug.wrappers.Request`, сопровождаемый всеми атрибутами,
   которые определены в Werkzeug.  Здесь дан лишь краткий обзор
   наиболее важных из них.


Объекты ответа
--------------

.. autoclass:: flask.Response
   :members: set_cookie, data, mimetype

   .. attribute:: headers

      Объект :class:`Headers`, представляющий заголовки ответа.

   .. attribute:: status

      Строка со статусом ответа.

   .. attribute:: status_code

      Статус ответа в виде целого числа.


Сессии
------

Если вы установили :attr:`Flask.secret_key` вы можете использовать сессии во 
Flask приложении. Сессия в основном используется для того, что бы запоминать 
информацию между запросами. Во Flask это реализовано при помощи подписанных 
cookies. Таким образом, пользователь может посмотреть на содержимое сессии, но 
не может изменить его пока он не знает секретный ключ, поэтому убедитесь, что 
ключ сложный и неопределяемый.

Для доступа к текущей сессии вы можете использовать объект :class:`session`:

.. class:: session

   Объект сессии работает почти как обычный словарь, с той разницей, что он 
   следит за изменениями.

   Это прокси.  За дополнительными сведениями обращайтесь к
   :ref:`notes-on-proxies`.

   Следующие атрибуты интресны:

   .. attribute:: new

      `True` если сессия новая, в ином случае - `False`.

   .. attribute:: modified

      `True` если объект сессии был изменен. Имейте ввиду, что изменения 
      изменяемых структур данных не отмечаются автоматически, в этой ситуации 
      вы должны явно установить значение атрибута в `True` самостоятельно.
      Вот пример::

          # это изменение не будет отмечено из-за изменения изменяемого объекта 
          # (это тип `list` - список).
          session['objects'].append(42)
          # так что укажем это самостоятельно
          session.modified = True

   .. attribute:: permanent

      Если установлен в `True` то сессия будет жить 
      :attr:`~flask.Flask.permanent_session_lifetime` секунд. По умолчанию 31 
      день. Если установлен в `False` (по умолчанию), сессия будет удалена как 
      только пользователь закроет окно браузера.


Интерфейс сессий
----------------

.. versionadded:: 0.8

Итерфейс сессий предоставляет простой способ замены имплементации сессий Flask.

.. currentmodule:: flask.sessions

.. autoclass:: SessionInterface
   :members:

.. autoclass:: SecureCookieSessionInterface
   :members:

.. autoclass:: SecureCookieSession
   :members:

.. autoclass:: NullSession
   :members:

.. autoclass:: SessionMixin
   :members:

.. autodata:: session_json_serializer

   Этот объект предоставляет методы сериализации и десериализации похожие на 
   simplejson, но так же конвертирует некоторые встроенные объекты Python, 
   которые обычно появляются в сессиях. На данный момент следующие расширенные 
   значения поддерживаются в JSON при дампе:

   -   :class:`~markupsafe.Markup` объекты
   -   :class:`~uuid.UUID` объекты
   -   :class:`~datetime.datetime` объекты
   -   :class:`tuple`\ы

.. admonition:: Замечание

   Ключ конфигурации ``PERMANENT_SESSION_LIFETIME`` может так же быть числом 
   начиная с версии Flask 0.8. Вы можете использовать атрибут 
   :attr:`~flask.Flask.permanent_session_lifetime` в приложении с автоматической 
   конвертацией результата в число.


Тестовый клиент
---------------

.. currentmodule:: flask.testing

.. autoclass:: FlaskClient
   :members:


Глобальные переменные
---------------------

.. currentmodule:: flask

Для совместного использования данных, которые действительны в течение одного 
запроса и только от одной функции к другой, глобальная переменная не достаточно 
хороша, потому что она нарушила бы работу приложения в многопоточных средах.
Flask предоставляет специальный объект, который гарантирует что для разных 
запросов будут возвращены разные значения. В двух словах: это работает примерно 
так же как и :class:`request` и :class:`session`.

.. data:: g

   Просто храните здесь все что хотите. Например соединение с базой данных или 
   пользователя, который сейчас авторизирован.

   Начиная с Flask 0.10 это хранится в контексте приложения и не зависит от 
   контекста запроса, что делает их доступными уже в момент создани контекста 
   приложения, но еще до создания запроса. Это особенно полезно когда 
   используется подход :ref:`faking-resources` для тестирования.
   
   Дополнительно с версии 0.10 вы можете использовать метод :meth:`get` для 
   получения значения атрибута или `None` (или второго аргумента метода 
   :meth:`get`) если значение отсутствует. Эти две строки эквивалентны::

        user = getattr(flask.g, 'user', None)
        user = flask.g.get('user', None)

   Так же допускается использование оператора ``in`` на этом объекте для 
   проверки на существование ключа в объекте.

   Это прокси.  За дополнительными сведениями обращайтесь к
   :ref:`notes-on-proxies`.


Полезные функции и классы
-------------------------

.. data:: current_app

   Приложение в текущем запросе. Это используется для расширений, 
   которые хотят поддерживать несколько приложений работающих бок о бок.   
   Это работает на основе контекста приложения а не на контексте запроса, так 
   что вы можете изменить значение этого прокси используя метод 
   :meth:`~flask.Flask.app_context`.

   Это прокси.  За дополнительными сведениями обращайтесь к
   :ref:`notes-on-proxies`.

.. autofunction:: has_request_context

.. autofunction:: copy_current_request_context

.. autofunction:: has_app_context

.. autofunction:: url_for

.. function:: abort(code)

   Вызывает исключение :exc:`~werkzeug.exceptions.HTTPException` для переданного 
   кода ответа. Например для отмены запроса и ответа со статусом 404 - страница 
   не найдена, вы можете вызвать ``abort(404)``.

   :param code: код ответа HTTP.

.. autofunction:: redirect

.. autofunction:: make_response

.. autofunction:: after_this_request

.. autofunction:: send_file

.. autofunction:: send_from_directory

.. autofunction:: safe_join

.. autofunction:: escape

.. autoclass:: Markup
   :members: escape, unescape, striptags

Передача сообщений
------------------

.. autofunction:: flash

.. autofunction:: get_flashed_messages

Поддержка JSON
--------------

.. module:: flask.json

Flask использует ``simplejson`` в качестве JSON имплементации. Flask будет 
пытаться использовать библиотеку simplejson, и в случае если ее нет - будет 
использовать стандартный JSON модуль. Поверх этого предоставляется доступ к JSON 
сериализаторам и десериализаторам текущего приложения для облегчения расширения.

Для начала попробуем следующее::

    try:
        import simplejson as json
    except ImportError:
        import json

Вместо этого можно просто сделать так::

    from flask import json

В качестве примера использования прочитайте документацию к :mod:`json` в 
стандартной библиотеке. Следующие расширения по умолчанию добавлены в 
стандартный JSON модуль:

1.  ``datetime`` объекты, которые могут быть сериализированы в строки 
    :rfc:`822`.
2.  Любые объекты, имеющие метод ``__html__`` (такие как :class:`~flask.Markup`
    ). Будет вызван метод ``__html__`` и возвращаемое значение будет 
    представлено как строка.

Функция :func:`~htmlsafe_dumps` этого json модуля также доступна через вызов 
фильтра ``|tojson`` в Jinja2. Заметьте, что внутри `script` тегов данные 
экранируются, так что убедитесь, что вы отключили экранирование с ``|safe`` если 
вы хотите исопльзовать внутри `script` теги:

.. sourcecode:: html+jinja

    <script type=text/javascript>
        doSomethingWith({{ user.username|tojson|safe }});
    </script>

.. admonition:: Автосортировка ключей JSON

    Переменная конфигурации ``JSON_SORT_KEYS`` (:ref:`config`) может быть 
    установлена в false для предотвращения автосортировки ключей в Flask. По 
    умолчанию сортировка включена и вне контекста приложения сортировка так же 
    включена.

    Заметьте, что отключение сортировки ключей может вызвать проблемы при 
    кэшировании результата.

.. autofunction:: jsonify

.. autofunction:: dumps

.. autofunction:: dump

.. autofunction:: loads

.. autofunction:: load

.. autoclass:: JSONEncoder
   :members:

.. autoclass:: JSONDecoder
   :members:

Отрисовка шаблонов
------------------

.. currentmodule:: flask

.. autofunction:: render_template

.. autofunction:: render_template_string

.. autofunction:: get_template_attribute

Конфигурация
------------

.. autoclass:: Config
   :members:

Расширения
----------

.. data:: flask.ext

   Этот модуль отвечает за перенаправление импорта к расширениям Flask. Он был 
   добавлен в версии 0.8 как канонический путь для импорта расширений Flask и 
   дает нам возможность создавать расширяемые и распространяемые дополнения.

   Если вы хотите использовать расширение с именем “Flask-Foo” вы должны 
   импортировать его из :data:`~flask.ext` как указано::

        from flask.ext import foo

   .. versionadded:: 0.8

Потоки
------

.. autofunction:: stream_with_context

Внутренние дополнения
---------------------

.. autoclass:: flask.ctx.RequestContext
   :members:

.. data:: _request_ctx_stack

   Внутренний класс :class:`~werkzeug.local.LocalStack` используется для 
   имплементации всех локальных контекстных объектов используемых в Flask. Это 
   документированный инстанс и может быть использован в коде расширений или 
   приложений но использовать его не рекомендуется.

   Следующие атрибуты всегда доступны на каждом слое стэка:

   `app`
      активное приложение Flask.

   `url_adapter`
      URL адптер который использвется для поиска соответствий запроса.

   `request`
      текущий объект запроса.

   `session`
      активный объект сессии.

   `g`
      объект со всеми атрибутами объекта :data:`flask.g`.

   `flashes`
      внутренний кэш для флеш сообщений.

   Пример использования::

      from flask import _request_ctx_stack

      def get_session():
          ctx = _request_ctx_stack.top
          if ctx is not None:
              return ctx.session

.. autoclass:: flask.ctx.AppContext
   :members:

.. data:: _app_ctx_stack

   Работает аналогично контексту запроса только привязан к приложению.
   Используется в основном расширениями для хранения данных.

   .. versionadded:: 0.9

.. autoclass:: flask.blueprints.BlueprintSetupState
   :members:

Сигналы
-------

.. when modifying this list, also update the one in signals.rst

.. versionadded:: 0.6

.. data:: signals_available

   `True` if the signaling system is available.  This is the case
   when `blinker`_ is installed.

.. data:: template_rendered

   This signal is sent when a template was successfully rendered.  The
   signal is invoked with the instance of the template as `template`
   and the context as dictionary (named `context`).

.. data:: request_started

   This signal is sent before any request processing started but when the
   request context was set up.  Because the request context is already
   bound, the subscriber can access the request with the standard global
   proxies such as :class:`~flask.request`.

.. data:: request_finished

   This signal is sent right before the response is sent to the client.
   It is passed the response to be sent named `response`.

.. data:: got_request_exception

   This signal is sent when an exception happens during request processing.
   It is sent *before* the standard exception handling kicks in and even
   in debug mode, where no exception handling happens.  The exception
   itself is passed to the subscriber as `exception`.

.. data:: request_tearing_down

   This signal is sent when the application is tearing down the request.
   This is always called, even if an error happened.  An `exc` keyword
   argument is passed with the exception that caused the teardown.

   .. versionchanged:: 0.9
      The `exc` parameter was added.

.. data:: appcontext_tearing_down

   This signal is sent when the application is tearing down the
   application context.  This is always called, even if an error happened.
   An `exc` keyword argument is passed with the exception that caused the
   teardown.  The sender is the application.

.. data:: appcontext_pushed

   This signal is sent when an application context is pushed.  The sender
   is the application.

   .. versionadded:: 0.10

.. data:: appcontext_popped

   This signal is sent when an application context is popped.  The sender
   is the application.  This usually falls in line with the
   :data:`appcontext_tearing_down` signal.

   .. versionadded:: 0.10

.. data:: message_flashed

   This signal is sent when the application is flashing a message.  The
   messages is sent as `message` keyword argument and the category as
   `category`.

   .. versionadded:: 0.10

.. currentmodule:: None

.. class:: flask.signals.Namespace

   An alias for :class:`blinker.base.Namespace` if blinker is available,
   otherwise a dummy class that creates fake signals.  This class is
   available for Flask extensions that want to provide the same fallback
   system as Flask itself.

   .. method:: signal(name, doc=None)

      Creates a new signal for this namespace if blinker is available,
      otherwise returns a fake signal that has a send method that will
      do nothing but will fail with a :exc:`RuntimeError` for all other
      operations, including connecting.

.. _blinker: http://pypi.python.org/pypi/blinker

Class-Based Views
-----------------

.. versionadded:: 0.7

.. currentmodule:: None

.. autoclass:: flask.views.View
   :members:

.. autoclass:: flask.views.MethodView
   :members:

.. _url-route-registrations:

URL Route Registrations
-----------------------

Generally there are three ways to define rules for the routing system:

1.  You can use the :meth:`flask.Flask.route` decorator.
2.  You can use the :meth:`flask.Flask.add_url_rule` function.
3.  You can directly access the underlying Werkzeug routing system
    which is exposed as :attr:`flask.Flask.url_map`.

Variable parts in the route can be specified with angular brackets
(``/user/<username>``).  By default a variable part in the URL accepts any
string without a slash however a different converter can be specified as
well by using ``<converter:name>``.

Variable parts are passed to the view function as keyword arguments.

The following converters are available:

=========== ===============================================
`string`    accepts any text without a slash (the default)
`int`       accepts integers
`float`     like `int` but for floating point values
`path`      like the default but also accepts slashes
=========== ===============================================

Here are some examples::

    @app.route('/')
    def index():
        pass

    @app.route('/<username>')
    def show_user(username):
        pass

    @app.route('/post/<int:post_id>')
    def show_post(post_id):
        pass

An important detail to keep in mind is how Flask deals with trailing
slashes.  The idea is to keep each URL unique so the following rules
apply:

1. If a rule ends with a slash and is requested without a slash by the
   user, the user is automatically redirected to the same page with a
   trailing slash attached.
2. If a rule does not end with a trailing slash and the user requests the
   page with a trailing slash, a 404 not found is raised.

This is consistent with how web servers deal with static files.  This
also makes it possible to use relative link targets safely.

You can also define multiple rules for the same function.  They have to be
unique however.  Defaults can also be specified.  Here for example is a
definition for a URL that accepts an optional page::

    @app.route('/users/', defaults={'page': 1})
    @app.route('/users/page/<int:page>')
    def show_users(page):
        pass

This specifies that ``/users/`` will be the URL for page one and
``/users/page/N`` will be the URL for page `N`.

Here are the parameters that :meth:`~flask.Flask.route` and
:meth:`~flask.Flask.add_url_rule` accept.  The only difference is that
with the route parameter the view function is defined with the decorator
instead of the `view_func` parameter.

=============== ==========================================================
`rule`          the URL rule as string
`endpoint`      the endpoint for the registered URL rule.  Flask itself
                assumes that the name of the view function is the name
                of the endpoint if not explicitly stated.
`view_func`     the function to call when serving a request to the
                provided endpoint.  If this is not provided one can
                specify the function later by storing it in the
                :attr:`~flask.Flask.view_functions` dictionary with the
                endpoint as key.
`defaults`      A dictionary with defaults for this rule.  See the
                example above for how defaults work.
`subdomain`     specifies the rule for the subdomain in case subdomain
                matching is in use.  If not specified the default
                subdomain is assumed.
`**options`     the options to be forwarded to the underlying
                :class:`~werkzeug.routing.Rule` object.  A change to
                Werkzeug is handling of method options.  methods is a list
                of methods this rule should be limited to (`GET`, `POST`
                etc.).  By default a rule just listens for `GET` (and
                implicitly `HEAD`).  Starting with Flask 0.6, `OPTIONS` is
                implicitly added and handled by the standard request
                handling.  They have to be specified as keyword arguments.
=============== ==========================================================

.. _view-func-options:

View Function Options
---------------------

For internal usage the view functions can have some attributes attached to
customize behavior the view function would normally not have control over.
The following attributes can be provided optionally to either override
some defaults to :meth:`~flask.Flask.add_url_rule` or general behavior:

-   `__name__`: The name of a function is by default used as endpoint.  If
    endpoint is provided explicitly this value is used.  Additionally this
    will be prefixed with the name of the blueprint by default which
    cannot be customized from the function itself.

-   `methods`: If methods are not provided when the URL rule is added,
    Flask will look on the view function object itself is an `methods`
    attribute exists.  If it does, it will pull the information for the
    methods from there.

-   `provide_automatic_options`: if this attribute is set Flask will
    either force enable or disable the automatic implementation of the
    HTTP `OPTIONS` response.  This can be useful when working with
    decorators that want to customize the `OPTIONS` response on a per-view
    basis.

-   `required_methods`: if this attribute is set, Flask will always add
    these methods when registering a URL rule even if the methods were
    explicitly overridden in the ``route()`` call.

Full example::

    def index():
        if request.method == 'OPTIONS':
            # custom options handling here
            ...
        return 'Hello World!'
    index.provide_automatic_options = False
    index.methods = ['GET', 'OPTIONS']

    app.add_url_rule('/', index)

.. versionadded:: 0.8
   The `provide_automatic_options` functionality was added.

`Оригинал этой страницы <http://flask.pocoo.org/docs/api/>`_